// Firebase App (the core Firebase SDK) is always required and must be listed first
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBMV2lVXYgZslYssQR344hYf6GFir8Z9r4",
    authDomain: "reacthelloworld-wtt.firebaseapp.com",
    projectId: "reacthelloworld-wtt",
    storageBucket: "reacthelloworld-wtt.appspot.com",
    messagingSenderId: "905094719499",
    appId: "1:905094719499:web:590535acf6f02e91e47f0e"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
/*const db = firebase.firestore();
const storage = firebase.storage();*/

export default firebase;