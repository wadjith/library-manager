import React from "react";
import firebase from "./Firebase";

const storage = firebase.storage();

class Photo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            imageUrl: '',
        };
    }

    componentDidMount() {
        
        if (this.props.imagePath) {
            const photoRef = storage.ref(this.props.imagePath);
            photoRef.getDownloadURL().then(url => {
                this.setState({
                    imageUrl: url
                })
                console.log(`Pour ${this.props.imagePath}, url = ${url}`)
            })
                .catch(error => {
                    console.error(error);
                });
        }
        
    }

    render() {
        const url = (this.state.imageUrl === '') ? "https://via.placeholder.com/150" : this.state.imageUrl;
        return(
            <img src={url} className="img-responsive" alt="Livre 1" />
        );
    }
}

export default Photo;