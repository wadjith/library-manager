import React from "react";
import firebase from "./Firebase";
import ListBooks from "./ListBooks";
import RegisterBook from "./RegisterBook";
import bookList from "./books";

const db = firebase.firestore();

class BookManager extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      selectedBook: {},
      books: [], 
      loading: true
      
    };

    this.searchBook = this.searchBook.bind(this);
    this.listBook = this.listBook.bind(this);
    this.addBook = this.addBook.bind(this);
    this.getAllBooks = this.getAllBooks.bind(this);
  }

  /*async componentDidMount() {
     try{
       const theBooks = await this.getAllBooks();

       this.setState({
        books: theBooks,
        loading: false
  
      });
      console.log(`Inside componentDidMount: ${theBooks.length}`);
     } catch(e) {
       console.error(e.message);

     }    
  }*/
  
  componentDidMount() {

    db.collection('Books').get().then((querySnapshot) => {
      const myBooks = [];
      querySnapshot.forEach(doc => {
        let bookDoc = {
          id: '',
          data: {}
        };
        bookDoc.id = doc.id;
        bookDoc.data = doc.data();
        myBooks.push(bookDoc)
      })
      this.setState({
        books: myBooks,
        loading: false
  
      });

    }).catch(error => console.error()`From ComponentDidMount: ${error}`);
  }

  listBook() {
    const myBooks = bookList;
    return myBooks;
  }

  getAllBooks() {

    return db.collection('Books').get().then((querySnapshot) => {
      const myBooks = [];
      querySnapshot.forEach(doc => myBooks.push(doc.data()))
      return myBooks;
    });

  }

  searchBook(searchText){
    const searchBooks = [...this.state.books].filter(book => book.data.title.includes(searchText)); 
    this.setState({
      books: searchBooks
    });

  }

  addBook(book) {
    // Ajout dans la liste des livres, le nouveau livre enregistré
    const myBooks = [book,...this.state.books];
    this.setState({
      books: myBooks
    });
    console.log(`List of books : ${this.state.books}`);
    
  }

  render() {
    
    const books = this.state.books;
    
    return (
      <div className="row">
        <div className="col-lg-8 col-md-8">
        { (this.state.loading) ? 
          <p>Chargement des livres en cours .... Veuillez patienter !!</p> : 
          <ListBooks bookList={books} searchBook={this.searchBook} /> }
        </div>
        <div className="col-lg-4 col-md-4">
          <RegisterBook addBook={this.addBook} />
        </div>
        
      </div>

    );
  }
}
export default BookManager;