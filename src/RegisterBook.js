import React from "react";
import firebase from "./Firebase";

const db = firebase.firestore();
// Réference au points de stockage de nos images
const imagesRef = firebase.storage().ref('images');

class RegisterBook extends React.Component {
    constructor(props){ 
        super(props);
        this.state = {
            title: '',
            isbn: '',
            description: '',
            editionYear: '',
            saveExecuted: false, // save has been executed ? 
            saveSucceed: false,  // save has been succeed ?
            pending: false,
        };

        this.fileInput = React.createRef();

        this.saveBook = this.saveBook.bind(this);
        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
     }

     componentDidMount(){
         
     }

     async saveBook(book) {
        const docRef = await db.collection('Books').add(book);
         console.log("Book added with ID : " + docRef.id);
         return docRef.id;
        
     }

     handleFocus(event) {
         // Je cache les notifications de succès ou échec
         if (this.state.saveExecuted) {
            this.setState({
                saveExecuted: false,
                saveSucceed: false,
            })
         }
         
     }
    
     handleChangeInput(event) {
        const target = event.target;
        const value = (target.type === "number") ? parseInt(target.value) : target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });

    }
/*
    handleSubmit(event) {
        event.preventDefault();
        alert(
            `Nombre de Fichier sélectionné - ${this.fileInput.current.files.length}`
        );
    }
*/
    handleSubmit(event) {
        event.preventDefault();

        // Je récupère les données sur le fichier sélectionné
        const file = (this.fileInput.current.files.length === 0) ? null : this.fileInput.current.files[0];

        const fileRef = imagesRef.child(file.name);
        const book = {
            title: this.state.title, 
            isbn: this.state.isbn, 
            description: this.state.description, 
            editionYear: this.state.editionYear,
            photo: fileRef.fullPath,
        };

        //Sauvegarde de l'image dans le storage
        fileRef.put(file).then((snapshot) => {
            console.log('Uploaded a blob or file!');
        })
        .then(() => {
            // J'enregistre le livre
            return db.collection('Books').add(book)
        })
        .then((docRef) => {
            console.log("Book added with ID : " + docRef.id);

            // Ajouter dans la liste des livres
            const bookDoc = {
                id: docRef.id,
                data: book
            }
            this.props.addBook(bookDoc);

            // Réinitialiser l'état 
            this.setState(
                {
                    title: '',
                    isbn: '',
                    description: '',
                    editionYear: 0,
                    saveExecuted: true,  
                    saveSucceed: true, 
                    pending: false, 
                });
        })
        .catch(error => {
            console.error(error);

            this.setState({
                saveExecuted: true,
                saveSucceed: false,
                pending: false,
            })
        });

        this.setState({pending: true});
        
    
        //TODO: Persister avec firebase sur firestore 
        /*
        try {
            this.saveBook(book);

            // Ajouter dans la liste des livres
            this.props.addBook(book);

            // Réinitialiser l'état 
            this.setState(
                {
                    title: '',
                    isbn: '',
                    description: '',
                    editionYear: 0,
                    saveExecuted: true,  
                    saveSucceed: true, 
                    pending: false, 
                });

        } catch (error) {
            console.error(error);
            this.setState({
                saveExecuted: true,
                saveSucceed: false,
                pending: false,
            })
        } */
        

    }

    render() {
        return(
            <div className="row">
                <h4>Enregistrer un nouveau Livre</h4>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="title">Titre du Livre</label>
                        <input type="text" className="form-control" id="title" placeholder="Titre du livre"
                            onChange={this.handleChangeInput} onFocus={this.handleFocus} value={this.state.title} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="isbn">ISBN</label>
                        <input type="text" className="form-control" id="isbn" placeholder="ISBN"
                            onChange={this.handleChangeInput} onFocus={this.handleFocus} value={this.state.isbn} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="annee">Année d'édition</label>
                        <input type="number" className="form-control" id="editionYear" placeholder="Année d'édition"
                            onChange={this.handleChangeInput} onFocus={this.handleFocus} value={this.state.editionYear} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="photos">Photo de couverture</label>
                        <input type="file" id="photos" name="photos" accept="images/.jpg, .jpeg, .png" ref={this.fileInput} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea className="form-control" rows="3" id="description" 
                            onChange={this.handleChangeInput} onFocus={this.handleFocus} value={this.state.description}></textarea>
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
                
                { this.state.pending && <div className="alert alert-info" role="alert">Enregistrement en cours ... </div> }

                { (this.state.saveExecuted && this.state.saveSucceed) && 
                    <div className="alert alert-success" role="alert">Enregistrement du livre effectué avec Success !!</div> }

                { (this.state.saveExecuted && !this.state.saveSucceed) && 
                    <div className="alert alert-danger" role="alert">Erreur lors de l'enregistrement du Livre</div> }
                
            </div>
            
        );
    }

    
}
export default RegisterBook;