import React from "react";

class TaskStatusUpdate extends React.Component {
    constructor(props) { 
        super(props);
        this.state = { value: 'nouveau' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
     }
    
     handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.fnUpdateStatusSelectedTask(this.state.value);
        console.log('status to change = ' + this.state.value)
    }

    render(){
        const pLabel = <p class="bg-primary">{this.props.selectedTaskLabel.toString()}</p>;
        const pEmpty = <p class="bg-warning">Vous devez sélectionner au mois une tâche ...</p>
        return(
            <div class="row">
                <h2>Changer le status des tâches</h2>
                {this.props.selectedTaskLabel.length === 0 ? pEmpty : pLabel}
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Appliquer le status :
                        <select class="form-control" value={this.state.value} onChange={this.handleChange}>
                            <option value="nouveau">Nouveau</option>
                            <option value="enCours">En Cours</option>
                            <option value="termine">Terminé</option>
                        </select>
                    </label>
                    <input type="submit" value="Changer" class="btn btn-default" disabled = {this.props.selectedTaskLabel.length === 0} />
                </form>
            </div>
            
        );
    }
    
}
export default TaskStatusUpdate;