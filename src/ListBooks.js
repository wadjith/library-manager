import React from "react";
import SearchBook from "./SearchBook";
import DisplayBooks from "./DisplayBooks";

class ListBooks extends React.Component{
   
    constructor(props){
        super(props);
        this.state = {
            loading: false,
        };

        
    }

    

    componentDidMount(){

    }

    render() {
        const bookList = this.props.bookList;
        const fnSearchBook = this.props.searchBook;

        return(
            <div>
                <h2>Liste des Livres</h2>
                <SearchBook fnSearchBook={fnSearchBook} />
                <DisplayBooks books={bookList}  />
                
            </div>
            
        );
    }
    
}

export default ListBooks;