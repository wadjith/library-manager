import React from "react";

class SearchBook extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            searchText: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            searchText: event.target.value
        });
        
    }

    handleSubmit(event){
        event.preventDefault();
        this.props.fnSearchBook(this.state.searchText)
    }

    render() {
        return (
            <div className="row">
                <form className="form-inline" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="searchBook">Titre du livre</label>
                        <input type="text" className="form-control" id="searchBook" placeholder="Entré un mot clé ..."
                            onChange={ this.handleChange } value={ this.state.searchText } required />
                    </div>
                    <button type="submit" className="btn btn-default">Chercher</button>
                </form>
                {this.state.searchText.length > 0 && <p className="help-block">Dont le titre contient: {this.state.searchText}</p>}
            </div>
        );
    }
    
    
}
export default SearchBook;