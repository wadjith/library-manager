import logo from './logo.svg';
import './App.css';
import BookManager from "./BookManager";

function App() {
  return (
    <BookManager />
  );
}

export default App;
