let bookList = [
    {title: "Titre 1", isbn: 60125874, description: "Prendre un seau", editionYear: 2016},
    {title: "Titre 2", isbn: 60125875, description: "Prendre un seau", editionYear: 2016},
    {title: "Titre 3", isbn: 60125876, description: "Prendre un seau", editionYear: 2016},
    {title: "Titre 4", isbn: 60125877, description: "Prendre un seau", editionYear: 2016},
    {title: "Titre 5", isbn: 60125878, description: "Prendre un seau", editionYear: 2016},
    {title: "Titre 6", isbn: 60125879, description: "Prendre un seau", editionYear: 2016},
];

export default bookList;