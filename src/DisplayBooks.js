import React from "react";
import Photo from "./Photo";

function DisplayBooks(props) {
    
    const { books } = props;
    
    return(
        <div className="row">
            {books.map(book => 
                <div key={book.id} className="col-lg-4 col-md-4 col-sm-2 col-xs-1">
                    <h4>{book.data.title}</h4>
                    <Photo imagePath={book.data.photo} />
                    <button type="button" className="btn btn-primary">Détails</button>
                </div>
            )}

        </div>
    );
}

export default DisplayBooks;